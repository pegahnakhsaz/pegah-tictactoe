import React, { Component } from "react";
import "./App.css";
import Board from "./components/Board";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import MoodIcon from '@material-ui/icons/Mood';
import TabletAndroidIcon from '@material-ui/icons/TabletAndroid';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

class App extends Component {

  constructor(props) {
    super();
    this.state = this.initialState();
  }

  initialState () {
    return {
      whoMoves: 'X',
      squares: Array(9).fill(null),
      winner: null,
      gameType: null
    };
  }

  reset () {
    this.setState(this.initialState())
  }

 
  select_Game_Type (num) {
    this.setState({gameType: num})
  }

  componentDidUpdate() {
 
    if (this.state.gameType === 1 && this.state.whoMoves === 'O') {
      this.computerMove(this.state.squares, this.state.whoMoves);
    }
  }

  computerMove(squares, player) {
    let newSquares = squares;
    const isEmtpySpot = newSquares.some(el => el === null);

    if (this.state.winner || !isEmtpySpot) {
      return
    }

    let emptySquares = [];
    newSquares.forEach((el, index) => el === null ? emptySquares.push(index) : null)

    setTimeout(() => {
      const randomIndex = Math.floor(Math.random() * emptySquares.length);
      newSquares[emptySquares[randomIndex]] = player;

      const winner = this.calculate_Winner(newSquares);
      const nextPlayer = player === 'X'? 'O' : 'X';

      this.setState({
        squares: newSquares,
        whoMoves: nextPlayer,
        winner: winner
      })
    }, 2000);
}

  handleClick(i) {
    const newSquares = this.state.squares;
    const player = this.state.whoMoves;
    const gameType = this.state.gameType;

    if (!gameType) {
      alert("Hello I am Pegah\nHow are you?\nplz select your game type"); 
      return
    }

    if (this.state.winner) {
      return
    }

    if (newSquares[i]) {
      return
    }
    if (player === 'O' && gameType === 1) {
      
      return;
    }
    newSquares[i] = player;
    const winner = this.calculate_Winner(newSquares);
    const nextPlayer = player === 'X'? 'O' : 'X';

    this.setState({
        whoMoves: nextPlayer,
        squares: newSquares,
        winner: winner
    })
  }
  render() {
    const winner = this.calculate_Winner(this.state.squares);
    const isEmtpySpot = this.state.squares.some(el => el === null);

    let status;
    if (winner) {
      status = 'Winner: ' + winner;
    } else if (!isEmtpySpot) {
        status = 'It\'s a draw!';
      } else {
        status = 'Next player: ' + this.state.whoMoves;
    }
    const gameTypeText = this.state.gameType === 1 ?
      'player vs computer' :
      this.state.gameType === 2? 'player vs player' : null;

    if (!gameTypeText) {
      status = 'Please select type of game.'
    }
    return (
      <div className="App" style={{    background: 'linear-gradient(45deg, #9900cc 10%, #660033 90%)',}}>
        <AppBar position="static" style={{    background: 'linear-gradient(45deg, #FE6B8B 30%, #EB1A6E 90%)',}}>
          <Toolbar>
            <Typography variant='h3' align='center' style={{flexGrow: 1 }}>
              <Box textAlign="center">PEGAH Tic Tac Toe</Box>
            </Typography>
          </Toolbar>
          
            <Button border="10px" style={{background: 'linear-gradient( 90deg,#660033 10% ,#303030 60%)',}}
             variant="contained" color='secondary'
              onClick= {() => this.select_Game_Type(2)}
              startIcon={<MoodIcon/>}
              endIcon={<MoodIcon/>}>..............................</Button>
            <Button top="20px" style={{background: 'linear-gradient( 90deg,#990099 10% ,#606060 60%)',}} 
            variant="contained" color='primary'
              onClick= {() => this.select_Game_Type(1)}
              startIcon={<MoodIcon/>}
              endIcon={<TabletAndroidIcon />}>..............................</Button>
              <Button size="large" style={{background: 'linear-gradient( 90deg,#FF1C1C 10% ,#D11B27 60%)',}} 
              variant="contained" className="reset" onClick= {() => this.reset()}> Reset / End Game </Button>

              
           
        </AppBar>

        <Typography variant='h5' style={{    background: 'linear-gradient(45deg, #FE6B8B 30%, #EB1A6E 90%)',}}>
          {gameTypeText &&
            <Box className='game-info' textAlign='center'>{gameTypeText}             
              </Box>}
          <Box className='game-info' textAlign="center"><KeyboardArrowUpIcon/>{status}<KeyboardArrowUpIcon/></Box>
        </Typography>

        <div className='game'>
          <Board
            squares={this.state.squares}
            handleClick={(i)=> this.handleClick(i)}
          />
         
        </div>

      </div>
    );
  
  }
  
  calculate_Winner(squares) {
    const lines =
      [[0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]];
      for (let i = 0; i< lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] &&
            squares[a] === squares[c]) {
              return squares[a];
        }
      }
      return null;
  }
}

export default App;
