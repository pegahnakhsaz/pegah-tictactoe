import React, { Component } from 'react';
import Button from '@material-ui/core/Button';

class Square extends Component {

  render() {
    const style={
        background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
        Shadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
        fontSize: 100,
        borderRadius: "100px",
        maxWidth: "100px",
        maxHeight: "100px",
        minWidth: "100px",
        minHeight: "100px",
        padding:"10px",
        margin:"2px",
        bordercolor: "#000",
        border:"10px"
    }
    return (
    <Button variant="contained" style={style}
        
        color={this.props.value === 'X' ? 'primary' :
            this.props.value === 'O' ? 'primary' : 'default'}
        onClick={() => this.props.onClick()}>

        {this.props.value || ''}
    </Button>
    )
  }
}

export default Square;
